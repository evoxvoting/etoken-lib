import Web3 from 'web3';

import engine from './engine';

var web3 = new Web3(engine);
export default web3;